# Linko MX - HBase Shell

Docker image of the Linko MX HBase Shell.

## Requirements

  - Docker 20

## Contributing

### Git config

```bash
$ git config --local "user.name" "myusernameatgitlab"
```

```bash
$ git config --local "user.email" "myemail@linko.mx"
```

## Building

### Local installation

To create the image use:

  - Standalone

```bash
$ docker run \
    -u $(id -u):$(grep -w docker /etc/group | awk -F\: '{print $3}') \
    --rm \
    -w $(pwd) \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v $(pwd):$(pwd) \
    -v ${HOME}/.m2:${HOME}/.m2 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    azul/zulu-openjdk-alpine:8u282 \
    ./mvnw -Djansi.force=true -ntp -P standalone -U clean package
```

  - Pseudo Distributed

```bash
$ docker run \
    -u $(id -u):$(grep -w docker /etc/group | awk -F\: '{print $3}') \
    --rm \
    -w $(pwd) \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v $(pwd):$(pwd) \
    -v ${HOME}/.m2:${HOME}/.m2 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    azul/zulu-openjdk-alpine:8u282 \
    ./mvnw -Djansi.force=true -ntp -P pseudo-distributed -U clean package
```

Then launch the HBase Shell with:

  - Standalone

```bash
$ docker run -d \
    --net=host \
    --rm \
    --name=hbase \
    registry.gitlab.com/linkomx/linkomx-hbase-standalone:2.2.7
```

```bash
$ docker run -it \
    --net=host \
    --rm \
    --name=hbase-shell \
    linkomx-hbase-shell:2.2.7
```
 
  - Pseudo distributed

```bash
$ docker run -d \
    --net=host \
    --rm \
    --name=zookeeper \
    zookeeper:3.5.6
```

```bash
$ docker run -d \
    --net=host \
    --rm \
    --name=hbase \
    --add-host=zk-1:127.0.0.1 \
    --add-host=zk-2:127.0.0.1 \
    --add-host=zk-3:127.0.0.1 \
    --add-host=$(hostname):127.0.0.1 \
    registry.gitlab.com/linkomx/linkomx-hbase-pseudo-distributed:2.2.7
```

```bash
$ docker run -it \
    --net=host \
    --add-host=zk-1:127.0.0.1 \
    --add-host=zk-2:127.0.0.1 \
    --add-host=zk-3:127.0.0.1 \
    --add-host=$(hostname):127.0.0.1 \
    --rm \
    --name=hbase-shell \
    linkomx-hbase-shell:2.2.7
```


